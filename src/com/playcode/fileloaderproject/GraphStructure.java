/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.playcode.fileloaderproject;

/**
 *
 * @author unknown
 */
public class GraphStructure {
    private int count;
    private String value;
    private String title;
    
    public GraphStructure (int count, String value, String title) {
        this.count = count;
        this.value = value;
        this.title = title;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    @Override
    public String toString(){
        return "GraphStructure { value: " + this.value 
            + ", \ncount: " + this.count 
            + "\ntitle: " + this.title + " }";
    }
    
}
