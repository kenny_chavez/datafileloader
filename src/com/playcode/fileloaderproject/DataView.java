/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.playcode.fileloaderproject;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Set;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author unknown
 */
public class DataView extends javax.swing.JInternalFrame {

    /**
     * Creates new form DataView
     */
    ArrayList<String> headers = new ArrayList<String>();
    ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
    DefaultTableModel model;
    DefaultListModel modelList;
    int sizeData;
    String fileType;
    GraphView graphView;
    int generalCount;
    
    public DataView() {
        initComponents();
    }
    
    public DataView(String path) {
        this.initComponents();
        model = (DefaultTableModel) mainTableData.getModel();
        
        if (path.contains(".json")) {
            this.loadJsonData(path);
            fileType = "json";
        } else if (path.contains(".csv")) {
            this.loadCsvData(path);
            fileType = "csv";
        } else {
            System.out.println("File type not accepted.");
        }
    }
    
    public void loadCsvData(String path) {
        int rows = 0; int size = 0;
        
        try{
            BufferedReader br = new BufferedReader(new FileReader(path));
            CSVParser csvParser = CSVFormat.DEFAULT.parse(br);
            
            for (CSVRecord record : csvParser) {
                ArrayList<String> internalData = new ArrayList<String>();
                rows++;
                
                for(short i = 0;i<record.size();i++){
                    if(rows == 1) {
                        headers.add(record.get(i));
                        size++;
                    } else {
                        internalData.add(record.get(i));
                    }
                }
                
                if(internalData.size() != 0) {
                    data.add(internalData);
                }
            }
            
            String[] columns = new String[headers.size()];
            columns = headers.toArray(columns);
            String[][] dataTable = new String[data.size()][size];
            model = new DefaultTableModel(null, columns);
            sizeData = size;
            
            for(int i = 0; i < data.size(); i++) {
                for (int a = 0; a < size; a++) {
                    dataTable[i][a] = data.get(i).get(a);
                }
                
                model.insertRow(model.getRowCount(), dataTable[i]);
            }
            
            this.createList();
            this.generalCount = this.data.size() - 1;
            mainTableData.setModel(model);
            mainTableData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            br.close();
        }catch(Exception e){
            System.out.println("An error has ocurred when loadCsvData function intend load data.");
            System.out.println("Exception: " + e);
        }
    }
    
    public void loadJsonData(String path) {
        JSONParser parser = new JSONParser();
        int count = 0;
        
        try {
            JSONArray mainArray = (JSONArray) parser.parse(new FileReader(path));
            Iterator<Object> iterator = mainArray.iterator();
            
            while (iterator.hasNext()) {
                JSONObject obj = (JSONObject) iterator.next();
                count++;
                
                if (count == 1) {
                    Set<String> allKeys = obj.keySet();
                    this.saveHeaders(allKeys);
                }
                
                ArrayList<String> internalData = new ArrayList<String>();
                
                obj.values().forEach(value -> {
                    internalData.add(value.toString());
                });
                
                data.add(internalData);
            }
            
            String[] columns = new String[this.sizeData];
            columns = headers.toArray(columns);
            String[][] dataTable = new String[mainArray.size()][this.sizeData];
            model = new DefaultTableModel(null, columns);            
            
            for(int i = 0; i < mainArray.size(); i++) {
                for (int a = 0; a < this.sizeData; a++) {
                    dataTable[i][a] = data.get(i).get(a);
                }
                
                model.insertRow(model.getRowCount(), dataTable[i]);
            }
            
            this.createList();
            this.generalCount = this.data.size();
            mainTableData.setModel(model);
            mainTableData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        }catch (Exception e) {
            System.out.println("An error has ocurred when loadJsonData function intend load data.");
            System.out.println("Error:"+ e);
        }
    }
    
    void saveHeaders(Set<String> allKeys) {
        int count = 0;
        for (String head: allKeys) {
            count++;
            this.sizeData = count;
            headers.add(head);
        }
    }
    
    void createList() {
        modelList = new DefaultListModel();
        
        for(String item: headers) {
            modelList.addElement(item);
        }
        
        listHeaders.setModel(modelList);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        mainTableData = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        listHeaders = new javax.swing.JList<>();
        btnCount = new javax.swing.JButton();
        btnSum = new javax.swing.JButton();
        btnAverage = new javax.swing.JButton();
        txtCount = new javax.swing.JTextField();
        txtSum = new javax.swing.JTextField();
        txtAverage = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnLineal = new javax.swing.JButton();
        btnPie = new javax.swing.JButton();
        btnBar = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);

        jLabel1.setText("Columnas");

        mainTableData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(mainTableData);

        jScrollPane2.setViewportView(listHeaders);

        btnCount.setText("Contar");
        btnCount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCountActionPerformed(evt);
            }
        });

        btnSum.setText("Suma");
        btnSum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSumActionPerformed(evt);
            }
        });

        btnAverage.setText("Promedio");
        btnAverage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAverageActionPerformed(evt);
            }
        });

        jLabel2.setText("Funciones Aritméticas");

        jLabel3.setText("Gráficas");

        btnLineal.setText("Lineal");
        btnLineal.setPreferredSize(new java.awt.Dimension(80, 27));
        btnLineal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLinealActionPerformed(evt);
            }
        });

        btnPie.setText("Pie");
        btnPie.setPreferredSize(new java.awt.Dimension(80, 27));
        btnPie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPieActionPerformed(evt);
            }
        });

        btnBar.setText("Barras");
        btnBar.setPreferredSize(new java.awt.Dimension(80, 27));
        btnBar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 782, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnCount, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnSum, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnAverage, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtCount)
                                    .addComponent(txtSum)
                                    .addComponent(txtAverage)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnLineal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnPie, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(btnBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCount)
                            .addComponent(txtCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSum)
                            .addComponent(txtSum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAverage)
                            .addComponent(txtAverage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnLineal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCountActionPerformed
        txtCount.setText(Integer.toString(this.generalCount));
    }//GEN-LAST:event_btnCountActionPerformed

    private void btnSumActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSumActionPerformed
        int selectedItem = listHeaders.getSelectedIndex();
        String selectedValue = listHeaders.getSelectedValue();
        int start = 0; double columnSum = 0; boolean error = false;
        
        if (selectedItem != -1) {
            if (selectedValue.toLowerCase().contains("date".toLowerCase()) || selectedValue.toLowerCase().contains("year".toLowerCase())){
                txtSum.setText("The field selected is a date");
            }else {
                if (fileType.equals("csv")) {
                    start = 1;
                } else if (fileType.equals("json")){
                    start = 0;
                }

                for (int a = start; a < data.size(); a++) {
                    try {
                        columnSum = columnSum + Double.parseDouble(data.get(a).get(selectedItem));
                    } catch (Exception e) {
                        System.out.println(e);
                        error = true;
                        break;
                    }
                }

                if (!error) {
                    txtSum.setText(Double.toString(columnSum));
                } else {
                    txtSum.setText("Error to convert this element on int");
                }
            }
        } else {
            txtSum.setText("No ha seleccionado ninguna columna.");
        }
    }//GEN-LAST:event_btnSumActionPerformed

    private void btnAverageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAverageActionPerformed
        int selectedItem = listHeaders.getSelectedIndex();
        String selectedValue = listHeaders.getSelectedValue();
        int start = 0; double columnSum = 0; boolean error = false; double average = 0;
        
        if (selectedItem != -1) {
            if (selectedValue.toLowerCase().contains("date".toLowerCase())){
                txtAverage.setText("The field selected is a date");
            }else {
                if (fileType.equals("csv")) {
                    start = 1;
                } else if (fileType.equals("json")){
                    start = 0;
                }

                for (int a = start; a < data.size(); a++) {
                    try {
                        columnSum = columnSum + Double.parseDouble(data.get(a).get(selectedItem));
                    } catch (Exception e) {
                        System.out.println(e);
                        error = true;
                        break;
                    }
                }

                if (!error) {
                    average = columnSum / (data.size() - start);
                    txtAverage.setText(Double.toString(average));
                } else {
                    txtAverage.setText("Error to convert this element on int");
                }
            }
        } else {
            txtAverage.setText("No ha seleccionado ninguna columna.");
        }
    }//GEN-LAST:event_btnAverageActionPerformed

    private void btnLinealActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLinealActionPerformed
        String title = listHeaders.getSelectedValue();
        int columnSelected = listHeaders.getSelectedIndex();
        ArrayList<GraphStructure> graphData = new ArrayList<GraphStructure>();
        
        for(int i = 0; i < data.size(); i++) {
            String value = data.get(i).get(columnSelected);
            boolean result = graphData.stream().anyMatch(g -> g.getValue().equals(value));
            
            if (!result) {
                GraphStructure gs = new GraphStructure(1, value, title);
                graphData.add(gs);
            } else {
                graphData.forEach(item -> {
                    if(item.getValue().equals(value)) {
                        int countValue = item.getCount() +1;
                        item.setCount(countValue);
                    }
                });
            }
        }
        
        graphView = new GraphView("line", graphData, title, generalCount);
        graphView.setTitle("Gráfica");
        graphView.setSize(1024, 728);
        graphView.setVisible(true);
    }//GEN-LAST:event_btnLinealActionPerformed

    private void btnPieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPieActionPerformed
        String title = listHeaders.getSelectedValue();
        int columnSelected = listHeaders.getSelectedIndex();
        ArrayList<GraphStructure> graphData = new ArrayList<GraphStructure>();
        
        for(int i = 0; i < data.size(); i++) {
            String value = data.get(i).get(columnSelected);
            boolean result = graphData.stream().anyMatch(g -> g.getValue().equals(value));
            
            if (!result) {
                GraphStructure gs = new GraphStructure(1, value, title);
                graphData.add(gs);
            } else {
                graphData.forEach(item -> {
                    if(item.getValue().equals(value)) {
                        int countValue = item.getCount() +1;
                        item.setCount(countValue);
                    }
                });
            }
        }
        
        graphView = new GraphView("pie", graphData, title, generalCount);
        graphView.setTitle("Gráfica");
        graphView.setSize(1024, 728);
        graphView.setVisible(true);
    }//GEN-LAST:event_btnPieActionPerformed

    private void btnBarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBarActionPerformed
        String title = listHeaders.getSelectedValue();
        int columnSelected = listHeaders.getSelectedIndex();
        ArrayList<GraphStructure> graphData = new ArrayList<GraphStructure>();
        
        for(int i = 0; i < data.size(); i++) {
            String value = data.get(i).get(columnSelected);
            boolean result = graphData.stream().anyMatch(g -> g.getValue().equals(value));
            
            if (!result) {
                GraphStructure gs = new GraphStructure(1, value, title);
                graphData.add(gs);
            } else {
                graphData.forEach(item -> {
                    if(item.getValue().equals(value)) {
                        int countValue = item.getCount() +1;
                        item.setCount(countValue);
                    }
                });
            }
        }
        
        graphView = new GraphView("bar", graphData, title, generalCount);
        graphView.setTitle("Gráfica");
        graphView.setSize(1024, 728);
        graphView.setVisible(true);
    }//GEN-LAST:event_btnBarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAverage;
    private javax.swing.JButton btnBar;
    private javax.swing.JButton btnCount;
    private javax.swing.JButton btnLineal;
    private javax.swing.JButton btnPie;
    private javax.swing.JButton btnSum;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList<String> listHeaders;
    private javax.swing.JTable mainTableData;
    private javax.swing.JTextField txtAverage;
    private javax.swing.JTextField txtCount;
    private javax.swing.JTextField txtSum;
    // End of variables declaration//GEN-END:variables
}
